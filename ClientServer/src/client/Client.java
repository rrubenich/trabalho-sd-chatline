package client;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.util.ArrayList;

import server.Services;

import dobject.Message;
import dobject.User;


public class Client implements Runnable{
	Login loginInterface;
	User  user = null;
	
	public Client(Login loginInterface){
		this.loginInterface = loginInterface;
	}

	@Override
	public void run() {
		try {
			final Services stub = (Services) Naming.lookup("rmi://192.168.1.12/chat");
			
			
			String userName = loginInterface.getUserName();
			user = new User(userName, null, 0);
			user = (User) stub.registry(user);
			final User userThread = user;
			loginInterface.closeWindow();
			
			
			final Runnable msgInterface = new Messenger(userThread, stub);
			msgInterface.run();
			
			
			//Atualiza mensagens
			new Thread(){
				public void run(){
					while(true){
						try {
							ArrayList<Message> messages = stub.getMessages(userThread);
							
							if(!messages.isEmpty()){
								((Messenger) msgInterface).AddMessages(messages);					
								userThread.setlastMessage(messages.get(messages.size() -1).getId());
							}
							
							sleep(2000);
						} catch (InterruptedException | RemoteException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
				}
			}.start();
			
			
			//Atualiza Usuarios Logados
			new Thread(){
				public void run(){
					while(true){
						try {
							ArrayList<User> users = stub.getUsers();
							((Messenger) msgInterface).SetUsers(users);
							sleep(1000);
						} catch (RemoteException | InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
					
				}
				
			}.start();
			
			//Remove Usuario deslogado
			Runtime.getRuntime().addShutdownHook(
				new Thread() {
				    public void run() {
				    	try {
							stub.unregistry(userThread);
						} catch (RemoteException | ServerNotActiveException e) {
							e.printStackTrace();
						}
				    }
				}
			);
			
			
		}catch (Exception e) {
			e.printStackTrace();
		}	
	}
}
