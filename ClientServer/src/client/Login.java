package client;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;

import server.Server;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Login extends Thread{

	private JFrame frame;
	private JTextField userNameTextField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);	
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 280, 129);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lblUsurio = new JLabel("Usuário");
		
		userNameTextField = new JTextField();
		userNameTextField.setColumns(10);
		
		JButton btnLigarServidor = new JButton("Ligar Servidor");
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0){
				startThread();
			}
		});
		btnEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0){
				try {
					new Server();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Erro ao iniciar Server \n" + e.getMessage());
				}
			}
		});
		
		
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(userNameTextField, GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnLigarServidor)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnEntrar, GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE))
						.addComponent(lblUsurio))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(lblUsurio)
					.addGap(3)
					.addComponent(userNameTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnLigarServidor)
						.addComponent(btnEntrar))
					.addContainerGap(164, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}
	public void startThread(){
		Runnable clientThread = new Client(this);
		clientThread.run();
	}
	
	public String getUserName(){
		return this.userNameTextField.getText();
	}
	
	public void closeWindow(){
		this.frame.setVisible(false);
	}
}