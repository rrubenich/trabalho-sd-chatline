package dobject;
import java.io.Serializable;
import java.rmi.RemoteException;

public class User implements Serializable{

	static final long serialVersionUID = 1L;
	
	String name;
	String ip;
	int lastMessage;
	
	
	public User(String name, String ip, int lastMessage) throws RemoteException {
		super();
		this.name = name;
		this.ip = ip;
		this.lastMessage = lastMessage;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getlastMessage() {
		return lastMessage;
	}
	public void setlastMessage(int lastMessage) {
		this.lastMessage = lastMessage;
	}
}