package dobject;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Message implements Serializable{
	private static final long serialVersionUID = 1L;

	int id;
	String message;
	User user;
	Date date;
	
	public Message(int id, String message, User user, Date date) {
		super();
		this.id = id;
		this.message = message;
		this.user = user;
		this.date = date;
	}
	
	public Message(){
		super();
	}

	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public Date getDate() {
		return date;
	}
	
	public String getFormatedTime() {
		SimpleDateFormat sf = new SimpleDateFormat("HH:mm"); 
		return sf.format(date);
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
}
