package server;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;


public class Server{
	
	public Server() throws RemoteException, MalformedURLException{
		
		java.rmi.registry.LocateRegistry.createRegistry(1099);
		System.setProperty("java.rmi.server.hostname", "192.168.1.12");
		Services stub = new ServicesImpl();
		Naming.rebind("rmi://192.168.1.12/chat", stub);
		
		System.out.println("Server ON");
	}
	
	public static void main(String args[]) {
		try{
			new Server();
		}catch(Exception e){
			System.out.println(e);
			e.printStackTrace();
		}
	}
}
