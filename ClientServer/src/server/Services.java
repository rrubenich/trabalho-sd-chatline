package server;
import java.util.ArrayList;
import java.util.List;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;

import dobject.Message;
import dobject.User;

public interface Services extends Remote{

	static final long serialVersionUID = 1L;
	
	User registry(User user) throws RemoteException, ServerNotActiveException;
	User unregistry(User user) throws RemoteException, ServerNotActiveException;
	ArrayList<Message> getMessages(User user) throws RemoteException;
	void sendMessage(Message message) throws RemoteException;
	ArrayList<User> getUsers() throws RemoteException;
}
