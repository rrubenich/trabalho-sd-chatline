package server;
import java.rmi.RemoteException;
import java.rmi.server.ServerNotActiveException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import dobject.Message;
import dobject.User;



public class ServicesImpl extends java.rmi.server.UnicastRemoteObject implements Services{

	private static final long serialVersionUID = 1L;
	
	int messagesLimit;
	ArrayList<User> users;
	ArrayList<Message> messages;
	String teste = new String();
	
	
	public ServicesImpl() throws RemoteException {
		super();
		users = new ArrayList<>();
		messages = new ArrayList<>();
		messagesLimit = 5;
		
		User user = new User("Servidor", null, 0);
		Message m = new Message(1, "Bem vindo ao chat", user, new Date());
		messages.add(m);
	}
	
	public User registry(User user) throws RemoteException, ServerNotActiveException {
		user.setlastMessage(0);
		user.setIp(getClientHost());
		users.add(user);
		System.out.println("Usuario " + user.getName() + " online");
		return users.get(users.size()-1);
	}
	
	public User unregistry(User user) throws RemoteException, ServerNotActiveException {
		User u = null;
		
		for(int i = 0; i < users.size(); i++){
			u = users.get(i);
			
			if(u.getName().equals(user.getName()) && u.getIp().equals(user.getIp())){
				users.remove(i);
				System.out.println("Usuario " + u.getName() + " offline");
				break;
			}
		}
		
		return u;
	}
	
	public ArrayList<User> getUsers() throws RemoteException {
		return (!users.isEmpty()) ? users : null;
	}

	public ArrayList<Message> getMessages(User user) throws RemoteException {
		
		ArrayList<Message> userMessages = new ArrayList<>();;
		int lastMsg = user.getlastMessage();
		
		for(Message m: messages){
			
			if(lastMsg < m.getId()){
				
				System.out.println(m.getMessage() + " enviada para "+ user.getName());
						
				userMessages.add(m);
				lastMsg = m.getId();
			
			}
		}
		
		return userMessages;
	}

	public void sendMessage(Message message) throws RemoteException {
		int id = messages.get(messages.size() - 1).getId() + 1;
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		message.setDate(new Date());
		message.setId(id);
		
		while(messages.size() >= messagesLimit){
			messages.remove(0);
		}
		
		messages.add(message);
	}
}
